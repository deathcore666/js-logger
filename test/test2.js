const logger = require('../index');
const logLevels = require('../constants/logLevels');

const unvalidConfigs = {
    contactPoints: ['192.168.0.166'],
    keyspace: 'logs',
    tableName: 'testlogs',
    component: "M1-1",
    logLevel: 3,
    localLogLimit: 300
};

logger.init(unvalidConfigs)
    .then(()=>{
        logger.logError('qweqwe', 'qweqwe');
    })
    .catch(err=>console.log('err:', err));